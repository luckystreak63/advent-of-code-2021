use std::str::FromStr;

pub fn part_2() {
    let data = std::fs::read_to_string("./data.txt").unwrap();
    let vec_data: Vec<i32> = data.split("\n").map(|str| i32::from_str(str.trim()).unwrap()).collect();

    let mut window_data = Vec::new();
    for (i, val) in vec_data.iter().enumerate() {
        if i + 2 >= vec_data.len() {
            break;
        }
        window_data.push(val + vec_data.get(i + 1).unwrap() + vec_data.get(i + 2).unwrap());
    }

    let mut count = 0;
    let result = window_data.iter().reduce(|accum, item| {
        if accum < item {
            count += 1;
        }
        item
    }
    );
    println!("Part 2: {}", count);
}

pub fn part_1() {
    let data = std::fs::read_to_string("./data.txt").unwrap();
    let vec_data: Vec<i32> = data.split("\n").map(|str| i32::from_str(str.trim()).unwrap()).collect();
    let mut count = 0;
    let result = vec_data.iter().reduce(|accum, item| {
        if accum < item {
            count += 1;
        }
        item
    }
    );
    println!("Part 1: {}", count);
}
