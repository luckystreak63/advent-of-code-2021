mod day1;
mod day2;

pub trait AdventChallenge {
    fn part1(&self);
    fn part2(&self);
}

fn main() {
    let advent_challenge = day2::Day2::default();
    advent_challenge.part1();
    advent_challenge.part2();
}

